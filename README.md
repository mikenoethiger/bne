A [python django](https://www.djangoproject.com/) implementation for the BNE backend. Written with help of [Writing your first Django app](https://docs.djangoproject.com/en/2.2/intro/tutorial01/).

## Developer Setup

1. [Install Django](https://docs.djangoproject.com/en/2.2/intro/install/)
2. (*) Create database structure (execute from project root): `python manage.py migrate`
3. Create admin user: `python manage.py createsuperuser`
4. Start local web server: `python manage.py runserver`

(*) an sqlite database (`db.sqlite3` file) is facilitated, no further db setup necessary.

The backend endpoint to consume data is available at `/backend`.  
The admin endpoint to manage data is available at `/admin`.

## Run (Docker)

1. Build Docker Image: `docker build -t bne .`
2. Run Image: `docker run -d -p 8000:8000 bne`
3. Access app on `localhost:8000`