from django.http import HttpResponse
from django.shortcuts import render
from .models import TipOfTheDay
from random import randint

# Create your views here.

def index(request):
    count = TipOfTheDay.objects.all().count()
    if count > 0:
        random = randint(0, count-1)
        tip = TipOfTheDay.objects.all()[random]
        return HttpResponse("<p>Tip of the day:</p> <p><b>" + tip.tip_text + "</b></p>")
    else:
        return HttpResponse("<p>No tip of the day available.</p>")
